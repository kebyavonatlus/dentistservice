﻿using DentistService.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DentistService.Models
{
    public class Visit
    {
        public int Id { get; set; }
        public VisitType VisitType { get; set; }
        public DateTime VisitDate { get; set; }
        public VisitStatus VisitStatus { get; set; }
        public decimal Summ { get; set; }

        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        [ForeignKey("User")]
        public int? UserId { get; set; }
        public virtual User User { get; set; }
    }
}
