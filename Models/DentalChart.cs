﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DentistService.Models
{
    public class DentalChart
    {
        public int Id { get; set; }
        public string Info { get; set; }
        public string Comment { get; set; }

        [ForeignKey("Teeth")]
        public int TeethId { get; set; }

        [ForeignKey("Visit")]
        public int VisitId { get; set; }

        public virtual Teeth Teeth { get; set; }
        public virtual Visit Visit { get; set; }
    }
}
