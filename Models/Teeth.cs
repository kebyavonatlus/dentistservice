﻿namespace DentistService.Models
{
    public class Teeth
    {
        public int Id { get; set; }
        public int JawType { get; set; }
        public int JawSide { get; set; }
        public int ToothNo { get; set; }
    }
}
