﻿namespace DentistService.Models
{
    public class Organization
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address  { get; set; }
        public string Supervisor { get; set; }
    }
}
