﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DentistService.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public bool IsBlocked { get; set; }
        public DateTime CreatedAt { get; set; }

        [ForeignKey("Organization")]
        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
