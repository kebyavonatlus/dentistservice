﻿using DentistService.Models;
using System;
using System.Linq;

namespace DentistService
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new DentistServiceContext();
            if (!context.Organizations.Any()
                    && !context.Patients.Any()
                    && !context.Visits.Any())
            {
                using var transaction = context.Database.BeginTransaction();
                var organization = new Organization
                {
                    Name = "Дент +",
                    Supervisor = "Шон",
                    Address = "Ахунбаева 10"
                };

                context.Organizations.Add(organization);
                context.SaveChanges();

                var patient = new Patient
                {
                    SurName = "Hawkins",
                    Name = "Mike",
                    Patronymic = "",
                    DateOfBirth = new DateTime(1990, 12, 1),
                    Gender = true,
                    CreatedAt = DateTime.Now,
                    OrganizationId = organization.Id
                };

                context.Patients.Add(patient);
                context.SaveChanges();

                var visit = new Visit
                {
                    PatientId = patient.Id,
                    Summ = 0,
                    VisitDate = DateTime.Now,
                    VisitStatus = Enums.VisitStatus.Created,
                    VisitType = Enums.VisitType.Inspection
                };

                context.Visits.Add(visit);
                context.SaveChanges();

                transaction.Commit();
            }
        }
    }
}
