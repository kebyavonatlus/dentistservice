﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DentistService.Enums
{
    public enum VisitType
    {
        [Display(Name = "Лечение")]
        Treatment = 1,

        [Display(Name = "Осмотр")]
        Inspection = 2
    }
}
