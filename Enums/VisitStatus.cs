﻿using System.ComponentModel.DataAnnotations;

namespace DentistService.Enums
{
    public enum VisitStatus
    {
        [Display(Name = "Создан")]
        Created = 1,

        [Display(Name = "Пришел на прием")]
        Came = 2,

        [Display(Name = "Не пришел на прием")]
        DoNotCame = 3
    }
}
